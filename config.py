import os

basedir = os.path.abspath(os.path.dirname(__file__))


SECRET_KEY = os.environ.get('SECRET_KEY') or 'galileo_galilei'
UPLOADED_FILES_DEST = "app/static/output_data/"
UPLOADED_FILES_ALLOW = ['.csv','.txt']
TESTING = True
ENV = 'development'
DEBUG = True
REDIS_URL = os.environ.get('REDIS_URL') or 'redis://'

# mail server settings
MAIL_SERVER = 'localhost'
MAIL_PORT = 8025
MAIL_USERNAME = None
MAIL_PASSWORD = None

# administrator list
ADMINS = ['myriama@gmail.com']
