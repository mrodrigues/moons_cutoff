from flask import Flask
from flask_bootstrap import Bootstrap
from flask_mail import Mail

from redis import Redis
import rq
from rq_scheduler import Scheduler
from datetime import datetime
import rq_dashboard

from config import basedir, ADMINS, MAIL_SERVER, MAIL_PORT, MAIL_USERNAME, \
    MAIL_PASSWORD

#initiate app
app = Flask(__name__, static_url_path='/templates/static')
app.config.from_object('config')
app.redis = Redis.from_url(app.config['REDIS_URL'])
app.task_queue = rq.Queue('cutoff-tasks', connection=app.redis)
app.register_blueprint(rq_dashboard.blueprint, url_prefix="/rq")

bootstrap = Bootstrap(app)
mail = Mail(app)
#print(MAIL_SERVER, MAIL_PORT)
from app import routes
from app import utils

scheduler = Scheduler('cutoff-cron', connection=app.redis)
#clean_job = app.task_queue.enqueue_in(timedelta(hours=24), remove_old_folders, dir_path)
test_job = scheduler.schedule(
    scheduled_time=datetime.utcnow(), # Time for first execution, in UTC timezone
    func=utils.print_message,                     # Function to be queued
    interval=10,                   # Time before the function is called again, in seconds
    repeat=None,                     # Repeat this number of times (None means repeat forever)
)
