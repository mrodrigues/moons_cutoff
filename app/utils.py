import time
import sys, os
from shutil import rmtree
from datetime import datetime, timedelta

def print_message():
    print("test message for scheduleur")

def remove_old_folders(dir_path):
    #all entries in the directory w/ stats
    archive_date =  datetime.today() - timedelta(hours=24, minutes=0)
    os.chdir(dir_path)
    list = [
        name for name in os.listdir('.')
        if os.path.isdir(name)
        and datetime.fromtimestamp(os.path.getmtime(name)) < archive_date
        ]
    for folder in list:
        rmtree(folder)
