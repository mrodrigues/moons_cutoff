from flask_wtf import FlaskForm
from wtforms import FloatField, StringField, FileField, FileField, BooleanField, SelectMultipleField, SubmitField
from wtforms.fields.html5 import EmailField
from wtforms.validators import DataRequired, Email, optional, length, NumberRange
from flask_wtf.file import  FileRequired, FileAllowed

import csv
import os

def load_filters():
    data =[]
    path = os.path.join(os.getcwd(), 'app/templates/')
    with open(path + 'filters_list.csv', newline='') as f:
        for line in csv.reader(f):
            filter = (line[1],line[0])
            data.append(filter)
    return data

class CutForm(FlaskForm):
    username = StringField('Name ', [DataRequired()])
    target_name = StringField('Target ', [optional(), length(max=40)])
    ra = FloatField('RA ')
    dec = FloatField('DEC ')
    filter_list = load_filters()
    filters = SelectMultipleField('Filters ', choices = filter_list )
    box = FloatField('Box size ', [DataRequired(), NumberRange(min = 4, max=100)])
    submit = SubmitField('Submit')

class CutListForm(FlaskForm):
    email = EmailField('Email ', [DataRequired(), Email()])
    username = StringField('Name ', [DataRequired()])
    file = FileField('File', validators=[FileRequired(),FileAllowed(['csv', 'txt'], 'CSV and TXT only!')])
    filter_list = load_filters()
    filters = SelectMultipleField('Filters ', choices = filter_list )
    box = FloatField('Box size ', [DataRequired(), NumberRange(min = 4, max=100)])
    submit = SubmitField('Submit')
