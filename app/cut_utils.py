import sys
import os
from matplotlib import pyplot as plt
import numpy as np

from astropy.io import ascii
from astropy.wcs import WCS
from astropy.wcs.utils import skycoord_to_pixel
from astropy.io import fits
from astropy.coordinates import SkyCoord, ICRS
from astropy.nddata import Cutout2D
import astropy.units as u

from astropy.coordinates import SkyCoord
import astropy.units as u

def read_targetfile(FileName):
    # Open variable-based csv, iterate over the rows and map values to a list of dictionaries containing key/value pairs
    path = os.path.join(os.getcwd(), 'app/static/output_data/')
    print(path+FileName)
    data = ascii.read(path+FileName)
    data.rename_column('col1', 'ra')
    data.rename_column('col2', 'dec')
    return data

def Coord2ID(RA,DEC, unit=[u.deg,u.deg], prefix = 'MOONS_'):

    c = SkyCoord(RA*unit[0],DEC*unit[0])
    ra_hh = str(int(c.ra.hms[0])).zfill(2)
    ra_mm = str(int(c.ra.hms[1])).zfill(2)
    ra_ss = str(round(c.ra.hms[2],100)).zfill(4)
    ra_format = "%s%s%s" % (ra_hh,ra_mm,ra_ss)

    sign ='+'
    if c.dec.dms[0] < 0:
      sign = '-'
    dec_dd = str(int(abs(c.dec.dms[0]))).zfill(2)
    dec_mm = str(int(abs(c.dec.dms[1]))).zfill(2)
    dec_ss = str(abs(round(c.dec.dms[2]*100))).zfill(4)
    dec_format = "%s%s%s%s" % (sign,dec_dd,dec_mm,dec_ss)

    ID = prefix+'%s%s' % (ra_format,dec_format)
    return ID


def get_pixel_pos(target_list,Reference_image):
    position=[]
    header = fits.getheader(Reference_image,0)
    wcs = WCS(header)
    for target in target_list:
        c_pixel= np.round(skycoord_to_pixel(target['coord'],wcs)).astype(int)
        if np.any(c_pixel < 0):
            position.append([-1,-1])
        else :
            position.append([c_pixel[0],c_pixel[1]])
    return(position)

def get_stamp_pics(Reference_image, path_out, target_list, coords_pixel, size=20, suffix ='filter', extension = 0 , save_fig=False):

    hdu = fits.open(Reference_image)[extension]
    wcs = WCS(hdu.header)
    size = u.Quantity(size, u.arcsec)

    i=0
    for target in  target_list:
       pos = coords_pixel[i]
    #for target, pos in zip(target_list,coords_pixel):
       file_out = path_out+target['name']+'_'+suffix
       cutout = Cutout2D(hdu.data, pos, size, wcs=wcs)

       # Put the cutout image in the FITS HDU
       hdu_stamp = fits.PrimaryHDU(cutout.data)
       hdu_stamp.header.update(cutout.wcs.to_header())
       hdu_stamp.header.update(cutout.wcs.to_header())
       hdu_stamp.header['Name'] = target['name']
       hdu_stamp.header['Filter'] = suffix
       hdu_stamp.header['Original'] = Reference_image
       hdu_stamp.writeto(file_out+".fits", overwrite=True)
       i=i+1

       if save_fig:
           fig = plt.figure()
           fig.add_subplot(111, projection = cutout.wcs)
           plt.imshow(cutout.data, origin='lower', cmap=plt.cm.viridis)
           plt.xlabel('RA')
           plt.ylabel('Dec')
           plt.title(target['name']+' | '+ suffix)
           plt.savefig(file_out+".png")

    if save_fig:
        plt.close('all')

def get_reference_image(filter_name):
    path = os.path.join(os.getcwd(), 'app/static/')
    data = ascii.read(path + 'list_images.csv')
    file = data[np.where(data['Name'] == filter_name)]['filename']
    return file


def stamp_target( param, save_fig=True):
    path_input = os.path.join(os.getcwd(), '../Data/Imagery/')
    size_arcsec = u.Quantity(param['box'], u.arcsec)  # arcsec wide box

    #create new target
    target_list =[]
    coord = SkyCoord(ra=param['ra'], dec=param['dec'],unit=(u.deg, u.deg))
    target_list.append({'name': param['target'], 'coord': coord })

    #loop on filters
    for filter in param['filters']:
        try:
            file_data = get_reference_image(filter)
            reference_image = path_input+file_data[0]
            coords_pixel = get_pixel_pos(target_list,reference_image)
            get_stamp_pics(reference_image, param['path'], target_list,coords_pixel,size=size_arcsec, suffix=filter,extension=0, save_fig=True)
            #pass
        except:
            continue

def stamp_list( param, save_fig=True):
    path_input = os.path.join(os.getcwd(), '../Data/Imagery/')
    size_arcsec = u.Quantity(param['box'], u.arcsec)  # arcsec wide box

    #create new target
    target_list =[]
    #
    table_list = read_targetfile(param['file'])
    for line in table_list :
        coord = SkyCoord(ra=line['ra'], dec=line['dec'],unit=(u.deg, u.deg))
        name = Coord2ID(line['ra'],line['dec'], unit=[u.deg,u.deg], prefix = 'MOONS_')
        target_list.append({'name': name, 'coord': coord })

    #loop on filters
    for filter in param['filters']:
        try:
            print(filter)
            file_data = get_reference_image(filter)
            reference_image = path_input+file_data[0]
            coords_pixel = get_pixel_pos(target_list,reference_image)
            get_stamp_pics(reference_image, param['path'], target_list,coords_pixel,size=size_arcsec, suffix=filter,extension=0, save_fig=True)
            print("------")
            #pass
        except:
            continue
