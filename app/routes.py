from flask import render_template,flash, redirect, url_for, session, request

from werkzeug.utils import secure_filename

from app import app

from app.forms import CutForm, CutListForm
from app.tasks import example, cut_target, get_urlfiles, cut_targetlist, check_inputfile

import os
import tempfile
import rq
import csv


@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html', title='Home')

@app.route('/cut', methods=['GET', 'POST'])
def cut():
    form = CutForm()
    if form.validate_on_submit():
        #job = app.task_queue.enqueue('app.tasks.example', 23)
        path = os.path.join(os.getcwd(), app.config['UPLOADED_FILES_DEST'])
        param = {'path':path,'username':form.username.data,'target':form.target_name.data,'ra':form.ra.data ,'dec':form.dec.data,'filters':form.filters.data, 'box':form.box.data}
        if not form.target_name.data:
            param['target'] =  "MOONS-%s-%s" % (str(param['ra']),str(param['dec']))
        job = app.task_queue.enqueue('app.tasks.cut_target',param, result_ttl=3600)
        id_job =job.get_id()
        job.meta['user'] = param['username']
        job.meta['target'] = param['target']
        job.meta['filter'] = param['filters']
        job.meta['box'] = param['box']
        job.save_meta()
        flash('Hello {}! The cutoff is running for your request'.format( form.username.data))
        return redirect(url_for('job_status', job_id = id_job, job_user = form.username.data))
    return render_template('cut.html',  title='Sign In', form=form)

@app.route('/cut_list', methods=['GET', 'POST'])
def cut_list():
    form = CutListForm()
    if form.validate_on_submit():
        path = os.path.join(os.getcwd(), app.config['UPLOADED_FILES_DEST'])
        uploaded_file = request.files['file']
        filename = secure_filename(uploaded_file.filename)
        uploaded_file.save(os.path.join(path, filename))
        # Test the format of the input file
        file_status = check_inputfile(filename)
        if file_status == -2 :
            flash('Wrong file format - Please provide two columns: RA and DEC', 'error')
            return render_template('cut_list.html',  title='Sign In', form=form)
        if file_status == -1 :
            flash('File too large - online cutoff is restricted to 100 targets', 'error')
            return render_template('cut_list.html',  title='Sign In', form=form)
        param = {'path':path,'username':form.username.data,'file': filename,'filters':form.filters.data, 'box':form.box.data}
        job = app.task_queue.enqueue('app.tasks.cut_targetlist',param, result_ttl=3600)
        id_job =job.get_id()
        job.meta['user'] = param['username']
        job.meta['filter'] = param['filters']
        job.meta['box'] = param['box']
        job.save_meta()
        flash('Hello {}! The cutoff is running for your request. A link to download the data will be sent to this email {}'.format( form.username.data, form.email.data))
        return redirect(url_for('job_status', job_id = id_job, job_user = form.username.data))
    return render_template('cut_list.html',  title='Sign In', form=form)


@app.route('/job_status/<string:job_id>/<string:job_user>')
def job_status(job_id = None, job_user = None):
    rq_job = rq.job.Job.fetch(job_id, connection=app.redis)
    status = rq_job.get_status()
    job_info = {'id':job_id, 'user' : job_user, 'status': status}
    #get the url of the output files
    list_url = get_urlfiles(job_id)
    folder = 'output_data/'+job_id+'/'
    #get the url of .zip
    folder_zip = 'output_data/'
    zip_url = ''
    if os.path.exists('app/static/output_data/'+job_id+'.zip'):
        zip_url = 'output_data/'+job_id+'.zip'
        print(zip_url)
    return render_template('job_status.html', title='Jobs', job_info = job_info,list_url = list_url, folder=folder, zip_url=zip_url)

@app.route('/job_list')
def job_list():
    jobsID_inqueue = app.task_queue.job_ids
    jobs = []
    for job_id in jobsID_inqueue :
        job = rq.job.Job.fetch(job_id, connection=app.redis)
        job_info = {'id':job_id, 'user':job.meta['user'],'target':job.meta['target'],'filter':job.meta['filter'],'box':job.meta['box'] }
        jobs.append(job_info)
    return render_template('job_list.html', title='Jobs', jobs = jobs)
