import time
import sys, os
from shutil import rmtree
from datetime import datetime, timedelta
from rq import get_current_job
from app.cut_utils import stamp_target, stamp_list, read_targetfile

import smtplib
from config import MAIL_SERVER, MAIL_PORT

from zipfile import ZipFile
import os
from os.path import basename
from astropy.io import ascii


def check_inputfile(FileName):
    try:
        target_list = read_targetfile(FileName)
        if len(target_list.keys()) != 2 :
            return -2
        elif len(target_list['ra'])>100 :
            return -1
    except:
        return -2
    else:
        return 1

def zip_folder(path,dirName):
    # create a ZipFile object
    print(dirName+'.zip')
    with ZipFile('app/static/output_data/'+dirName+'.zip', 'w') as zipObj:
   # Iterate over all the files in directory
        for folderName, subfolders, filenames in os.walk(path+dirName):
            for filename in filenames:
                filePath = os.path.join(folderName, filename)
                print(filePath)
                zipObj.write(filePath, basename(filePath))

def send_email(subject, sender, recipients, text_body):
    headers = """From: %s To: %s Subject: Hello cutoff""" % (sender, ", ".join(recipients))
    message = headers + "\n" + text_body
    smtpObj = smtplib.SMTP('localhost', 8025)
    smtpObj.sendmail(sender, recipients, message)

def example(seconds):
    job = get_current_job()
    print('Starting task')
    for i in range(seconds):
        job.meta['progress'] = 100.0 * i / seconds
        job.save_meta()
        print(i)
        time.sleep(1)
    job.meta['progress'] = 100
    job.save_meta()
    print('Task completed')

def get_urlfiles(pathdir_job,extension='.png'):
    path = os.path.join(os.getcwd(), 'app/static/output_data/')
    pathdir = path + pathdir_job
    url =[]
    for root, dirs, files in os.walk(pathdir):
        for file in files:
            if file.endswith(extension):
                #print(os.path.join(root, file))
                url.append(file)
    return url

def cut_target(param):
    job = get_current_job()
    id_job =job.get_id()
    path_folder = param['path']
    param['path'] = param['path']+id_job+"/"
    os.mkdir(param['path'])
    stamp_target( param, save_fig=True)
    zip_folder(path_folder,id_job)
    #send_email('MOONS cutoff ready','myriama@gmail.com',['myriama@gmail.com'],"""Done""")
    return param['path']

def cut_targetlist(param):
    job = get_current_job()
    id_job =job.get_id()
    path_folder = param['path']
    param['path'] = param['path']+id_job+"/"
    os.mkdir(param['path'])
    stamp_list( param, save_fig=True)
    zip_folder(path_folder,id_job)
    #send_email('MOONS cutoff ready','myriama@gmail.com',['myriama@gmail.com'],"""Done""")
