FROM python:3.6-alpine

RUN adduser -D cutoff_web

WORKDIR /home/moons_cutoff

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
RUN pip install gunicorn

COPY app app
COPY cutoff_web.py config.py boot.sh ./
RUN chmod a+x boot.sh

ENV FLASK_APP cutoff_web.py

RUN chown -R cutoff_web:cutoff_web ./
USER cutoff_web

EXPOSE 5000
ENTRYPOINT ["./boot.sh"]
